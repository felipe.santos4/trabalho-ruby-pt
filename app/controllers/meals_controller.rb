class MealsController < ApplicationController
    before_action :set_meal, only: [:show, :update, :destroy]
    
    #GET /meals
    def index
        @meals = Meal.all
        render json: @meals, status: 200
    end

    # ----------------------------------------------------
    
    #GET /meals/search
    def search
        @q = Meal.ransack(name_cont:params[:q])
        @meals = @q.result(distinct: true)
        render json: @meals
    end
    
    # ----------------------------------------------------

    #GET /meals/:id
    def show
        if @meal.food_image.attached?
            @image = url_for(@meal.food_image)
        else
            @image = "This Meal doesn't have a image"
        end
        render json: @meal.attributes.merge!(url: @image), status: 200
    end

    # ----------------------------------------------------

    #POST /meals
    def create
        @meal = Meal.new(meal_params)
        
        if @meal.save
            render json: @meal, status: 201
        else
            render json: @meal.errors, status:400
        end
    end

    # ----------------------------------------------------

    #PUT/PATCH /meals/:id
    def update
        if @meal.update(meal_params)
            render json: @meal, status: 200
        else
            render json: @meal.errors
        end
    end

    # ----------------------------------------------------
    
    #DELETE /meals/:id
    def destroy
        @meal.destroy
        render json: { message: "Meal deleted" }
    end

    # ----------------------------------------------------

    private

    def set_meal
        @meal = Meal.find(params[:id])
    end

    def meal_params
        params.permit(:name, :description, :price, :available, :meal_category_id, :food_image)
    end
    
end
