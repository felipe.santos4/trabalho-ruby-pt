class MealCategoriesController < ApplicationController
    before_action :set_meal_c, only: [:show, :update, :destroy]

    # GET /meal_categories
    def index
        @meal_categories = MealCategory.all
        render json: @meal_categories, status: 200
    end

    # ----------------------------------------------------

    # GET /meal_categories/search
    def search
        @q = MealCategory.ransack(name_cont: params[:q])
        @m_category = @q.result(distinct: true)
        render json: @m_category
    end

    # ----------------------------------------------------

    # GET /meal_categories/:id
    def show
        render json: @meal_category, status: 200
    end

    # ----------------------------------------------------

    # POST /meal_categories
    def create
        @meal_category = MealCategory.new(meal_category_params)
        if @meal_category.save
            render json: @meal_category, status: 201
        else
            render json: @meal_category.errors
        end
    end

    # ----------------------------------------------------
    
    # GET /meal_categories
    def update
        if @meal_category.update(meal_category_params)
            render json: @meal_category, status: 200
        else
            render json: @meal_category.errors
        end
    end

    # ----------------------------------------------------
    
    # DELETE /meal_categories
    def destroy
        @meal_category.destroy
        render json: { message: "Meal Category deleted" }
    end

    # ----------------------------------------------------

    private

    def set_meal_c
        @meal_category = MealCategory.find(params[:id])
    end

    def meal_category_params
        params.require(:meal_category).permit(:name)
    end

end
