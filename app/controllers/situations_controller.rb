class SituationsController < ApplicationController
    before_action :set_situations, only: [:show, :update, :destroy]

    # GET /situations
    def index
        @situations = Situation.all
        render json: @situations, status: 200
    end

    # ----------------------------------------------------

    # GET /situations/:id
    def show
        render json: @situation, status: 200
    end

    # ----------------------------------------------------

    # POST /situations
    def create
        @situation = Situation.new(situation_params)
        if @situation.save
            render json: @situation, status: 201
        else
            render json: @situation.errors
        end
    end
    
    # ----------------------------------------------------

    # GET /situations
    def update
        if @situation.update(situation_params)
            render json: @situation, status: 200
        else
            render json: @situation.errors
        end
    end

    # ----------------------------------------------------
    
    # DELETE /situations
    def destroy
        @situation.destroy
        render json: { message: "Situation deleted" }
    end

    # ----------------------------------------------------

    private

    def set_situations
        @situation = Situation.find(params[:id])
    end

    def situation_params
        params.require(:situation).permit(:description)
    end
    
end
