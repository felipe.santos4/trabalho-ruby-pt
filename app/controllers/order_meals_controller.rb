class OrderMealsController < ApplicationController
    before_action :set_order_m, only: [:show, :update, :destroy]

    # GET /order_meals
    def index
        @order_meals = OrderMeal.all
        render json: @order_meals, status: 200
    end

    # ----------------------------------------------------

    # GET /order_meals/:id
    def show
        render json: @order_meal, status: 200
    end

    # ----------------------------------------------------

    # POST /order_meals
    def create
        @order_meal = OrderMeal.new(order_meal_params)
        
        # Relação entre OrderMeals e Order
        #@order = params[:order]
        #@selected_order = Order.find_by(id:@order)
        #@order_meal.order = @selected_order

        # Relação OrderMeal e Meal
        #@meal = params[:meal]
        #@selected_meal = Meal.find_by(name:@meal)
        #@order_meal.meal = @selected_meal

        if @order_meal.save
            render json: @order_meal, status: 201
        else
            render json: @order_meal.errors
        end
    end

    # ----------------------------------------------------
    
    # GET /order_meals
    def update
        if @order_meal.update(order_meal_params)
            render json: @order_meal, status: 200
        else
            render json: @order_meal.errors
        end
    end

    # ----------------------------------------------------
    
    # DELETE /order_meals
    def destroy
        @order_meal.destroy
        render json: { message: "Order meal deleted" }
    end

    # ----------------------------------------------------

    private

    def set_order_m
        @order_meal = OrderMeal.find(params[:id])
    end

    def order_meal_params
        params.permit(:quantity, :order_id, :meal_id)
    end

end
