class UsersController < ApplicationController
    before_action :authorize_request, except: :create
    before_action :set_user, only: [:show, :update, :destroy]
    before_action :find_user, except: %i[create index]

    #GET "/users"
    def index
        @users = User.all
        render json: @users, status: 200
    end

    # ----------------------------------------------------

    #GET "/users/:id"
    def show
        render json: @user, status: 200
    end
    
    # ----------------------------------------------------

    #POST "/users"
    def create
        @user = User.new(user_params)
        if @user.save
            render json: @user, status: 201
        else
            render json: @user.errors
        end
    end

    # ----------------------------------------------------

    #PUT/PATCH "/users/:id"
    def update
        if @user.update(user_params)
            render json: @user, status: 200
        else
            render json: @user.errors
        end
    end

    # ----------------------------------------------------

    #DELETE "/users/:id"
    def destroy
        @user.destroy
        render json: { message: "User deleted" }
    end

    # ----------------------------------------------------

    private

    def find_user
        @user = User.find_by_name!(params[:_name])
        rescue ActiveRecord::RecordNotFound
          render json: { errors: 'User not found' }, status: :not_found
      end

    def set_user
        @user = User.find(params[:id])
    end

    def user_params
        params.require(:user).permit(:name, :email, :password, :admin)
    end

end
