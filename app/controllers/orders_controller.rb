class OrdersController < ApplicationController
    before_action :set_order, only: [:show, :update, :destroy]

    #GET /orders
    def index
        @orders = Order.all
        render json: @orders, status: 200
    end

    # ----------------------------------------------------

    #GET /orders/:id
    def show
        # @order.total_price
        render json: @order, status: 200
    end

    # ----------------------------------------------------

    #POST /orders
    def create
        @order = Order.new(order_params)
        
        #relação entre order e user
        #@user = params[:user]
        #@selected_user = User.find_by(name:@user)
        #@order.user = @selected_user

        #realação entre order e situations
        #@situation = params[:situation]
        #@selected_situation = Situation.find_by(description:@situation)
        #@order.situation = @selected_situation

        if @order.save
            render json: @order, status: 201
        else
            render json: @order.errors
        end
    end

    # ----------------------------------------------------

    #PUT/PATCH /orders/:id
    def update
        if @order.update(order_params)
            
            render json: @order, status: 200, message: @price
        else
            render json: @order.errors
        end
    end
    
    # ----------------------------------------------------

    #DELETE /orders/:id
    def destroy
        @order.destroy
        render json: { message: "Order deleted" }
    end

    # ----------------------------------------------------

    private

    def set_order
        @order = Order.find(params[:id])
    end

    def order_params
        params.permit(:price, :user_id , :situation_id)
    end

end
