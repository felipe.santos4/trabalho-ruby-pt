class User < ApplicationRecord
    
    has_secure_password
    validates :name, presence: true, uniqueness:true, length: { maximum: 20 }
    validates :email, presence: true, uniqueness: true
    validates :password,
                presence: true,
                length: { in: 8..20 },
                if: -> { new_record? || !password.nill? },
                confirmation: true

    # ----------------------------------------------------

    # Relações
    has_many :orders
    
end
