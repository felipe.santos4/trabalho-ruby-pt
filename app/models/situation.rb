class Situation < ApplicationRecord
    # Validações
    validates :description, presence: false

    # ----------------------------------------------------

    # Relações
    has_many :orders
    
end
