class Meal < ApplicationRecord
    
    # Validações
    validates :name, presence: true
    validates :description, presence: true
    validates :price, presence: true, numericality:true
    validates :available, presence: true

    # ----------------------------------------------------

    # Relações
    belongs_to :meal_category
    has_many :ordermeals

    # ----------------------------------------------------
    
    # Imagem
    has_one_attached :food_image

end
