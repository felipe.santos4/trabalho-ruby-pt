class OrderMeal < ApplicationRecord

    # CallBacks para atualizar o preço
    after_create :total_price
    after_destroy :total_price

    # Validações
    validates :quantity, presence: true, numericality: true

    # ----------------------------------------------------
    
    # Relações
    belongs_to :order
    belongs_to :meal

    # ----------------------------------------------------

    # Método para atualizar o preço
    def total_price
        total_price = 0
        unless self.order.order_meals.nil?
            self.order.order_meals.each do |order|
                total_price += order.meal.price * order.quantity
            end
        end
        self.order.update(price: total_price)
    end
    
end
