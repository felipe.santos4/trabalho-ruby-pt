class MealCategory < ApplicationRecord

    # Validações
    validates :name, presence:true, length: { maximum: 20 }

    # ----------------------------------------------------
    
    # Relações
    has_many :meals
    
end
