Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # Users
  get "/users", to: "users#index"
  get "/users/:id", to: "users#show"
  post "/users", to: "users#create"
  patch "/users/:id", to: "users#update" 
  put "/users/:id", to: "users#update"
  delete "/users/:id", to: "users#destroy"

  # ----------------------------------------------------

  # Situations
  get "/situations", to: "situations#index"
  get "/situations/:id", to: "situations#show"
  post "/situations", to: "situations#create"
  patch "/situations/:id", to: "situations#update" 
  put "/situations/:id", to: "situations#update"
  delete "/situations/:id", to: "situations#destroy"

  # ----------------------------------------------------

  # Orders
  get "/orders", to: "orders#index"
  get "/orders/:id", to: "orders#show"
  post "/orders", to: "orders#create"
  patch "/orders/:id", to: "orders#update" 
  put "/orders/:id", to: "orders#update"
  delete "/orders/:id", to: "orders#destroy"

  # ----------------------------------------------------

  # Order_Meals
  get "/order_meals", to: "order_meals#index"
  get "/order_meals/:id", to: "order_meals#show"
  post "/order_meals", to: "order_meals#create"
  patch "/order_meals/:id", to: "order_meals#update" 
  put "/order_meals/:id", to: "order_meals#update"
  delete "/order_meals/:id", to: "order_meals#destroy"

  # ----------------------------------------------------

  # Meals
  get "/meals", to: "meals#index"
  get "/meals/search", to: "meals#search"
  get "/meals/:id", to: "meals#show"
  post "/meals", to: "meals#create"
  patch "/meals/:id", to: "meals#update" 
  put "/meals/:id", to: "meals#update"
  delete "/meals/:id", to: "meals#destroy"

  # ----------------------------------------------------

  # Meal_categories
  get "/meal_categories", to: "meal_categories#index"
  get "/meal_categories/search", to: "meal_categories#search"
  get "/meal_categories/:id", to: "meal_categories#show"
  post "/meal_categories", to: "meal_categories#create"
  patch "/meal_categories/:id", to: "meal_categories#update" 
  put "/meal_categories/:id", to: "meal_categories#update"
  delete "/meal_categories/:id", to: "meal_categories#destroy"

  # ----------------------------------------------------

  # JWT
  Rails.application.routes.draw do
    resources :users, param: :_username
    post '/auth/login', to: 'authentication#login'
    get '/*', to: 'application#not_found'
  end

end
